import React from 'react'
import { useAuth } from 'react-oauth2-pkce'

export const Home = () => {
  const { authService } = useAuth()

  const login = async () => {
    authService.authorize()
  }
  const logout = async () => {
    authService.logout()
  }

  if (authService.isPending()) {
    return <div>
      Loading...
      <button onClick={() => { logout(); login(); }}>Reset</button>
    </div>
  }

  if (!authService.isAuthenticated()) {
    return (
      <div>
        <button onClick={login}>Login</button>
      </div>
    )
  }

  //const token = authService.getAuthTokens();

  return (
    <div>
      <p>Logged in!</p>
      <button onClick={logout}>Logout</button>
    </div>
  )
}

export default Home
