import React from 'react'
import { AuthProvider, AuthService } from 'react-oauth2-pkce'

import { Routes } from './Routes';

//"https://localhost:5001/oauth2"
//"https://ssg-oauth2.vaultofkundarak.com/oauth2"

const authService = new AuthService({
  clientId: "275ea23c-c39f-4697-a041-b91785ce28d2",
  location: window.location,
  provider: "https://ssg-oauth2.vaultofkundarak.com/oauth2",
  redirectUri: process.env.REACT_APP_REDIRECT_URI || window.location.origin + "/",
  scopes: ["openid", "ddo", "subscriptions"],
  state: "gobbledegoop"
});

const App = () => {
  return (
    <AuthProvider authService={authService} >
      <Routes />
    </AuthProvider>
  )
}

export default App
